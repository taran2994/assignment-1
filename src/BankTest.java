import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class BankTest {

	@Test
	public void testInitCustomers() {    //B1 Checking number of customers = 0 when bank is created
		Bank bank = new Bank();
		int numberOfCustomers= bank.getNumberOfCustomers();
		assertEquals(0,numberOfCustomers);
	}

	@Test
	public void testAddCustomers() {    //B2 checking if we can Add customers
		Bank bank = new Bank();
		int initialNumberOfCusstomers = bank.getNumberOfCustomers();
		bank.addCustomer("Peter", 800);
		bank.addCustomer("Taran", 1800);
		int numberOfCustomers= bank.getNumberOfCustomers();
		assertEquals(initialNumberOfCusstomers+2,numberOfCustomers);
	}
	
	@Test
	public void testRemoveCustomers() {    //B3 Checking if we can remove customers 
		Bank bank = new Bank();
		int initialNumberOfCusstomers = bank.getNumberOfCustomers();
		bank.addCustomer("Peter", 800);
		bank.addCustomer("Taran", 1800);
		bank.addCustomer("Navpreet", 2800);
		int numberOfCustomers= bank.getNumberOfCustomers();
		bank.removeCustomer("Taran");
		assertEquals(initialNumberOfCusstomers+2,numberOfCustomers);
	}
	
	@Test
	public void testTransferBalance() {    //B4 Checking if we can transfer balance between two accounts
		Bank bank = new Bank();
		bank.addCustomer("Peter", 800);
		bank.addCustomer("Taran", 1800);
		ArrayList<Customer> list= bank.getCustomers();
	    Customer customer1= list.get(0);
	    Customer customer2= list.get(1);
		bank.transferMoney(customer1,customer2,400);
		
		Account accountOfCustomer1= customer1.getAccount();
		Account accountOfCustomer2= customer2.getAccount();
		
		int balanceOfCustomer1= accountOfCustomer1.balance();
		int balanceOfCustomer2= accountOfCustomer2.balance();
		assertEquals(2200, balanceOfCustomer2); // checking balance of customer 2 after transfer
		assertEquals(400, balanceOfCustomer1);  // checking balance of customer 1 after transfer
		
		
	}
	
}
