import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {

	@Test
	public void testAccountStartsWithZero() {   //A1 All new accounts are initialized with 0 dollars in the account.

		
		Account account = new Account(80);
		int currentBalance= account.balance();
		assertEquals(0, currentBalance);
	}

	@Test
	public void testDeposit()  //A2 & A4 checking if we can deposit in an account and balance updates immediately
	{
		Account account = new Account(80);
		int initialBalance= account.balance();
		account.deposit(800);
		int currentBalance = account.balance();
		assertEquals(initialBalance+800,currentBalance);
		
	}
	
	@Test
	public void testWithdraw()    //A2 & A3 checking if we can withdraw from an account and balance updates immediately
	{
		Account account = new Account(800);
		int initialBalance= account.balance();
		account.withdraw(400);
		int currentBalance = account.balance();
		assertEquals(initialBalance-400,currentBalance);
		
	}
	
	@Test
	public void testOverdraft()    //A3 checking if we can withdraw more money than the balance
	{
		Account account = new Account(400);
		int initialBalance= account.balance();
		account.withdraw(800);
		int currentBalance = account.balance();
		assertEquals(initialBalance-800,currentBalance);
		
	}
	
	
	
	
}

    