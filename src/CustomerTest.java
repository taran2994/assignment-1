import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	@Test
	public void testEveryCustomerHasAccount() {     //C1 Every customer has an account.
		Customer customer = new Customer("Taran",800);
		String nameOfAccount= customer.getName();
		assertEquals("Taran", nameOfAccount);
		
	}
	
	
	//New customers can choose to make an optional initial deposit into their account. If the customer chooses to deposit at the time of account creation, the customer starts with that amount. Otherwise, the customer starts with $0 in the account.


	@Test
	public void testInitialDeposit() {     //C2 Checking when customer makes an Initial deposit.
		Customer customer = new Customer("Taran",800);
		Account account= customer.getAccount();
		int currentBalance= account.balance();
		assertEquals(800,currentBalance);
		
	}
	
	@Test
	public void testInitialDepositZero() {     //C2 Checking when customer does not make an Initial deposit.
		Customer customer = new Customer("Taran",0);
		Account account= customer.getAccount();
		int currentBalance=0;
	try{	currentBalance= account.balance();
	}
	catch(Exception E)
	{
		fail("The case has failed");
	}
	
	assertEquals(0,currentBalance);
		
	}
}
